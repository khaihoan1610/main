﻿using System;
using System.Collections.Generic;
using System.Text;

namespace project_LTM
{
    class Cons
    {
        public static int CHESS_WIDTH = 30;
        public static int CHESS_HEIGHT = 30;
        public static int BOARD_WIDTH = 18;
        public static int BOARD_HEIGHT = 17;

        public static int COOL_DOWN_STEP = 100;
        public static int COOL_DOWN_TIME = 10000;
        public static int COOL_DOWN_INTERVAL = 100;//step process
    }
}
