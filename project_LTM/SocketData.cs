﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace project_LTM
{
    [Serializable]
    public class SocketData
    {
        private int command;

        public int Command
        {
            get { return command; }
            set { command = value; }
        }

        private Point point;

        public Point Point
        {
            get { return point; }
            set { point = value; }
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public SocketData(int command, string message, Point point)
        {
            this.Command = command;
            this.Point = point;
            this.Message = message;
        }
    }

    public enum SocketCommand 
    {
        SEND_POINT,// -> 0
        NOTIFY, // -> 1 ...
        NEW_GAME,
        UNDO,
        END_GAME,
        QUIT
    }
}
